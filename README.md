# README #

This repo is for the Android application Time-a-light. The app is for controlling 
a physical light-tower via the audio jack.

![Light Tower](lightTower.jpeg)

Play Store
https://play.google.com/store/apps/details?id=com.agbullet.mcsilvio.timealight&hl=en