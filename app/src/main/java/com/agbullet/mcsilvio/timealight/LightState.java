package com.agbullet.mcsilvio.timealight;

public enum LightState {
    IDLE,
    GREEN,
    YELLOW,
    RED;

    /**
     * Returns the state of the Light that is after the given one.
     * RED is the last state, and calls to this method while providing "RED"
     * as a parameter will simply return RED (ie. no cycling states)
     * @return The state of the Light after the given one.
     */
    public static LightState getNextState(LightState state){
        switch (state) {
            case IDLE:
                return LightState.GREEN;
            case GREEN:
                return LightState.YELLOW;
            case YELLOW:
                return LightState.RED;
            case RED:
            default:
                return state;
        }
    }
}
