package com.agbullet.mcsilvio.timealight;

import android.media.AudioTrack;

public enum Channel {
    NONE(AudioTrack.getMinVolume(), AudioTrack.getMinVolume()),
    LEFT(AudioTrack.getMaxVolume(), AudioTrack.getMinVolume()),
    RIGHT(AudioTrack.getMinVolume(), AudioTrack.getMaxVolume()),
    BOTH(AudioTrack.getMaxVolume(), AudioTrack.getMaxVolume());

    private float leftVolume;
    private float rightVolume;

    // constructor
    private Channel(float leftVolume, float rightVolume)
    {
        this.leftVolume = leftVolume;
        this.rightVolume = rightVolume;
    }

    /**
     * Returns the volume of the left channel
     * @return Returns the volume of the left channel
     */
    public float getLeftVolume()
    {
        return leftVolume;
    }

    /**
     * Returns the volume of the right channel
     * @return Returns the volume of the right channel
     */
    public float getRightVolume()
    {
        return rightVolume;
    }
}
