package com.agbullet.mcsilvio.timealight;

/**
 * Singleton class representing the one, physical light-tower
 */
public class LightTower
{
	private static LightTower singletonInstance = null;

	private LightState lightState = LightState.IDLE;
	private BuzzerState buzzerState = BuzzerState.OFF;
	private TonePlayer tonePlayer = null;

	/**
	 * Returns the singleton instance of the Light Tower
	 * @return The singleton instance of the Light Tower
	 */
	public static LightTower getInstance(){
		if (singletonInstance == null) {
			singletonInstance = new LightTower();
			singletonInstance.tonePlayer = TonePlayer.getInstance();
		}

		return singletonInstance;
	}

	/**
	 * Resets and initializes the Light Tower instance for future use
	 * @param buzzerState The desired state of the Light Tower buzzer
	 */
	public void initializeAndReset(BuzzerState buzzerState){
		this.lightState = LightState.IDLE;
		this.buzzerState = buzzerState;
		this.tonePlayer.resetAndInitialize();
	}

	/**
	 * Advances the state of the Tower's Light with respect to whether the Buzzer is ON or OFF.
	 * Since the RED is the last state the tower can be in, calls to this method while in a RED state
	 * will do nothing (i.e. will not cycle/reset).
	 */
	public void advanceLightState(){
		LightState newLightState = LightState.getNextState(lightState);
		enterNewLightState(newLightState);
	}

	/**
	 * Sets the Tower's Light state
	 * @param desiredLightState The desired state of the Tower's Light
	 * @return The resulting state of the Tower's Light
	 */
	public void setLightState(LightState desiredLightState) {
		enterNewLightState(desiredLightState);
	}

	/**
	 * Sets the Tower's Buzzer state
	 * @param desiredBuzzerState The desired state of the Tower's Buzzer
	 * @return The resulting state of the Tower's Buzzer
	 */
	public void setBuzzerState(BuzzerState desiredBuzzerState) {
		enterNewBuzzerState(desiredBuzzerState);
	}

	/**
	 * Handles entering the Tower's new Light state
	 * @param desiredLightState The desired state of the Tower's Light
	 * @return The resulting state of the Tower's Light
	 */
	private void enterNewLightState(LightState desiredLightState) {
		enterNewStateCombination(desiredLightState, buzzerState);
	}

	/**
	 * Handles entering the Tower's new Buzzer state
	 * @param desiredBuzzerState The desired state of the Tower's Buzzer
	 * @return The resulting state of the Tower's Buzzer
	 */
	private void enterNewBuzzerState(BuzzerState desiredBuzzerState) {
		enterNewStateCombination(lightState, desiredBuzzerState);
	}

	private boolean givenStateCombinationIsSameAsCurrent(LightState newLightState, BuzzerState newBuzzerState) {
		return newLightState == lightState && newBuzzerState == buzzerState;
	}

	private void enterNewStateCombination(LightState newLightState, BuzzerState newBuzzerState) {
		if (givenStateCombinationIsSameAsCurrent(newLightState, newBuzzerState)){
			return;
		}

		lightState = newLightState;
		buzzerState = newBuzzerState;

		Tone tone = Tone.mapLightAndBuzzerStateToTone(lightState, buzzerState);

		tonePlayer.StopCurrentTone();
		tonePlayer.PlayTone(tone);


	}
}
