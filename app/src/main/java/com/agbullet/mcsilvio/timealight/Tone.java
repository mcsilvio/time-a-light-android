package com.agbullet.mcsilvio.timealight;

public enum Tone {

    IDLE_NO_BUZZER(LightState.IDLE, BuzzerState.OFF, Channel.NONE),
    IDLE_WITH_BUZZER(LightState.IDLE, BuzzerState.ON, Channel.NONE),
    GREEN_NO_BUZZER(LightState.GREEN, BuzzerState.OFF, Channel.LEFT),
    GREEN_WITH_BUZZER(LightState.GREEN, BuzzerState.ON, Channel.BOTH),
    YELLOW_NO_BUZZER(LightState.YELLOW, BuzzerState.OFF, Channel.LEFT),
    YELLOW_WITH_BUZZER(LightState.YELLOW, BuzzerState.ON, Channel.BOTH),
    RED_NO_BUZZER(LightState.RED, BuzzerState.OFF, Channel.LEFT),
    RED_WITH_BUZZER(LightState.RED, BuzzerState.ON, Channel.BOTH);

    private int frequencyLeft;
    private int frequencyRight;
    private Channel channel;

    // constructor
    private Tone(LightState lightState, BuzzerState buzzerState, Channel channel)
    {
        /*
         * These should be static, but Java won't allow it in this enum. Compromise.
         */
        int BUZZER_FREQUENCY = 1000;
        int IDLE_FREQUENCY = 0;
        int GREEN_FREQUENCY = 1000;
        int YELLOW_FREQUENCY = 2000;
        int RED_FREQUENCY = 3000;

        switch (lightState) {
            default:
            case IDLE:
                frequencyLeft = IDLE_FREQUENCY;
                break;
            case GREEN:
                frequencyLeft = GREEN_FREQUENCY;
                break;
            case YELLOW:
                frequencyLeft = YELLOW_FREQUENCY;
                break;
            case RED:
                frequencyLeft = RED_FREQUENCY;
                break;
        }

        if (buzzerState == BuzzerState.ON) {
            frequencyRight = BUZZER_FREQUENCY;
        }

        this.channel = channel;
    }

    public static Tone mapLightAndBuzzerStateToTone(LightState lightState, BuzzerState buzzerState){
        switch (buzzerState) {
            case ON:
                switch (lightState) {
                    default:
                    case IDLE:
                        return IDLE_WITH_BUZZER;
                    case GREEN:
                        return GREEN_WITH_BUZZER;
                    case YELLOW:
                        return YELLOW_WITH_BUZZER;
                    case RED:
                        return RED_WITH_BUZZER;
                }
            case OFF:
            default:
                switch (lightState) {
                    default:
                    case IDLE:
                        return IDLE_NO_BUZZER;
                    case GREEN:
                        return GREEN_NO_BUZZER;
                    case YELLOW:
                        return YELLOW_NO_BUZZER;
                    case RED:
                        return RED_NO_BUZZER;
                }
        }
    }

    // return left channel frequency as integer
    public int getFrequencyLeft()
    {
        return frequencyLeft;
    }

    // return right channel frequency as integer
    public int getFrequencyRight()
    {
        return frequencyRight;
    }

    public Channel getChannel(){ return channel; }
}
