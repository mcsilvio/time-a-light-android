package com.agbullet.mcsilvio.timealight;

/**
 * Util class for woking with strings (e.g. "00:10") whose meaning are times
 */
public class TimeUtils {

    private static final int SECONDS_IN_MINUTE = 60;
    private static final int MAX_SECONDS_VALUE = 59;
    public static final int MAX_GREEN_SECONDS = 5997;
    private static final int GUARANTEED_VALID_STRING_LENGTH = 5;

    public static int getTimeInSeconds(final String timeText) throws NumberFormatException{

        // separate time string in the form of "00:10" (e.g.)
        String minutesText = timeText.substring(0, 2);
        String secondsText = timeText.substring(3, 5);

        int minutes = Integer.parseInt(minutesText);
        int seconds = Integer.parseInt(secondsText);

        int totalTimeInSeconds = minutes * SECONDS_IN_MINUTE + seconds;
        return totalTimeInSeconds;
    }

    public static boolean isValidTimeString(final String timePref) {
        if(timePref.isEmpty() || timePref.length() != GUARANTEED_VALID_STRING_LENGTH ){
            return false;
        }

        boolean isValidSoFar = true;
        try{
            getTimeInSeconds(timePref);
        } catch (NumberFormatException e){
            isValidSoFar = false;
        }

        if(isValidSoFar){
            String secondsText = timePref.substring(3, 5);
            int seconds = Integer.parseInt(secondsText);
            return seconds <= MAX_SECONDS_VALUE;
        } else {
            return false;
        }
    }


    public static boolean isValidTimeProfile(final String greenTime, final String yellowTime, final String redTime) {
        if(! isValidTimeString(greenTime) || !isValidTimeString(yellowTime) || !isValidTimeString(redTime)){
            return false;
        }

        int greenTimeSeconds, yellowTimeSeconds, redTimeSeconds;
        try {
            greenTimeSeconds = getTimeInSeconds(greenTime);
            yellowTimeSeconds = getTimeInSeconds(yellowTime);
            redTimeSeconds = getTimeInSeconds(redTime);
        } catch (NumberFormatException e){
            return false;
        }

        if(timeTooHighForGreen(greenTimeSeconds)){
            return false;
        }

        return timesAreIncreasing(greenTimeSeconds, yellowTimeSeconds, redTimeSeconds);
    }

    public static boolean timesAreIncreasing(int greenTimeSeconds, int yellowTimeSeconds, int redTimeSeconds) {
        return (greenTimeSeconds < yellowTimeSeconds && yellowTimeSeconds < redTimeSeconds);
    }

    public static boolean timeTooHighForGreen(final int greenTime){
        return greenTime > MAX_GREEN_SECONDS;
    }

    public static String getTimeAsStringFromInt(int totalSeconds) {
        int minutesOnly = totalSeconds / SECONDS_IN_MINUTE;
        int secondsOnly = totalSeconds % SECONDS_IN_MINUTE;
        String minutes = String.format("%02d", minutesOnly);
        String seconds = String.format("%02d", secondsOnly);

        return minutes + ":" + seconds;
    }
}
