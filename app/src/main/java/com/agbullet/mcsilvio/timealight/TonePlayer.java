package com.agbullet.mcsilvio.timealight;

import java.util.HashMap;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

//This is a TonePlayer which is responsible for generating and playing continuous
//sine waves of various frequencies
public class TonePlayer
{
	private static TonePlayer singletonInstance = null;
	private static final int DURATION_SECONDS = 1;
    private static final int SAMPLE_RATE = 22000;
	private Tone currentTone = null;
	


	// collection of audio tracks with Tone enum as key
	private HashMap<Tone, AudioTrack> audioTracks = null;

	// constructor
	private TonePlayer()
	{
		audioTracks = new HashMap<Tone, AudioTrack>();

		//pre generate all audio tracks
		initializeAllTones();
	}

	public static TonePlayer getInstance(){
		if (singletonInstance == null) {
			singletonInstance = new TonePlayer();
		}

		return singletonInstance;
	}

	public void resetAndInitialize(){

	}

	//generates all of the test tones that dont exist
	public void initializeAllTones()
	{
		if(!audioTracks.containsKey(Tone.GREEN_WITH_BUZZER))
			generateAudioTrack(Tone.GREEN_WITH_BUZZER, true);
		
		if(!audioTracks.containsKey(Tone.GREEN_NO_BUZZER))
			generateAudioTrack(Tone.GREEN_NO_BUZZER, true);
		
		if(!audioTracks.containsKey(Tone.YELLOW_WITH_BUZZER))
			generateAudioTrack(Tone.YELLOW_WITH_BUZZER, true);
		
		if(!audioTracks.containsKey(Tone.YELLOW_NO_BUZZER))
			generateAudioTrack(Tone.YELLOW_NO_BUZZER, true);
		
		if(!audioTracks.containsKey(Tone.RED_WITH_BUZZER))
			generateAudioTrack(Tone.RED_WITH_BUZZER, true);
		
		if(!audioTracks.containsKey(Tone.RED_NO_BUZZER))
			generateAudioTrack(Tone.RED_NO_BUZZER, true);
			
	}

    // generates an AudioTack containing a looping sine wave of the given
	// frequency and stores it.
	public void generateAudioTrack(Tone tone, boolean save)
	{
		// store values
		int numSamples = SAMPLE_RATE * DURATION_SECONDS;
		int frequencyLeft = tone.getFrequencyLeft();
		int frequencyRight = tone.getFrequencyRight();


        double leftSample[] = new double[numSamples];
        double rightSample[] = new double[numSamples];


		// audio buffer after conversion (for two channels)
		int bufferMultiplier = 4;
		byte[] generatedTrack = new byte[numSamples * bufferMultiplier];


		// generate the left channel wave
		for (int i = 0; i < numSamples; i++) {
			leftSample[i] = Math.sin(2f * Math.PI * (float) i / ((float) SAMPLE_RATE / (float) frequencyLeft));
		}

		// generate the right channel wave (if getBuzzerState is on)
		//for (int i = 0; i < numSamples / 4; i++)
		for (int i = 0; i < numSamples; i++) {
			rightSample[i] = Math.sin(2f * Math.PI * (float) i / ((float) SAMPLE_RATE / (float) frequencyRight));
		}

		// convert to 16 bit pcm sound array
		// assumes the sample buffer is normalised.
		int idx = 0;
		int MAXIMUM_AMPLITUDE = 32767;
		for (int x = 0; x < leftSample.length; x++) {
			//left channel
			// scale to maximum amplitude
			final short leftVal = (short) ((leftSample[x] * MAXIMUM_AMPLITUDE));
			// in 16 bit wav PCM, first byte is the low order byte
			generatedTrack[idx++] = (byte) (leftVal & 0x00ff);
			generatedTrack[idx++] = (byte) ((leftVal & 0xff00) >>> 8);

			// handle right channel
			// scale to maximum amplitude and put in array
			final short rightVal = (short) ((rightSample[x] * MAXIMUM_AMPLITUDE));
			// in 16 bit wav PCM, first byte is the low order byte
			generatedTrack[idx++] = (byte) (rightVal & 0x00ff);
			generatedTrack[idx++] = (byte) ((rightVal & 0xff00) >>> 8);
		}

		// configure audio track
		AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, generatedTrack.length, AudioTrack.MODE_STATIC);
        audioTrack.write(generatedTrack, 0, generatedTrack.length);
        audioTrack.setPlaybackHeadPosition(numSamples / frequencyLeft);
        audioTrack.setStereoVolume(tone.getChannel().getLeftVolume(), tone.getChannel().getRightVolume());
        audioTrack.setLoopPoints(0, numSamples, -1);


		// store audio track
		if(save) {
			audioTracks.put(tone, audioTrack);
		}
	}

	// plays a tone. if an audio track for this tone doesn't exist, this will
	// first create it.
	public void PlayTone(Tone tone)
	{
		if(audioTracks.containsKey(tone)){
			// set up channel

			// get the audio track
			AudioTrack audioTrack = audioTracks.get(tone);

			audioTrack.play();
			
			//store as current tone
			currentTone = tone;
		}
	}

	//stops the given audio track immediately
	private void StopAudioTrack(AudioTrack audioTrack)
	{
		// stop it
		audioTrack.flush();
		audioTrack.stop();
		audioTrack.release();
	}

	// stops all existing audio tracks
	public void StopAllTones()
	{
		// loop through collection of audio tracks, stopping each one
		for (Tone tone : audioTracks.keySet())
		{
			// get audio track
			AudioTrack audioTrack = audioTracks.get(tone);

			// stop the track
			StopAudioTrack(audioTrack);
		}

		// clear collection
		audioTracks.clear();
		
		//reset current tone
		currentTone = null;
	}
	
	//stops the current tone
	public void StopCurrentTone()
	{
		if(currentTone != null){
			
			// get audio track from collection
			if (audioTracks.containsKey(currentTone))
			{
				// get audio track and stop it
				AudioTrack audioTrack = audioTracks.get(currentTone);
				StopAudioTrack(audioTrack);

				// remove it from collection
				audioTracks.remove(currentTone);
				
				initializeAllTones();
				
				currentTone = null;
			}			
		}
	}

	// resets the tone player to an initialized state
	// must only be called when not playing
	public void Reset()
	{
		// clear the collection
		audioTracks.clear();
		initializeAllTones();
	}
}
