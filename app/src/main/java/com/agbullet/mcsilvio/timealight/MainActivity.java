package com.agbullet.mcsilvio.timealight;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Main class representing the primary execution point of the application
 */
public class MainActivity extends Activity implements Chronometer.OnChronometerTickListener {

    private AudioManager audio;
    private int originalVolume;

    private static final String PREFERENCE_NAME = "TimeProfile";
    private static final String GREEN_TIME_KEY = "GREEN_TIME";
    private static final String YELLOW_TIME_KEY = "YELLOW_TIME";
    private static final String RED_TIME_KEY = "RED_TIME";
    private static final String BUZZER_KEY = "BUZZER";
    private static final String DEFAULT_GREEN_TIME = "00:05";
    private static final String DEFAULT_YELLOW_TIME = "00:10";
    private static final String DEFAULT_RED_TIME = "00:15";
    private int greenTimeCache, yellowTimeCache, redTimeCache;

    private enum StopWatchState { IDLE, RUNNING_COLD, RUNNING_HOT, STOPPED }
    private enum AppState { EDIT, PLAY }

    private long pausedTime;
    private boolean buzzerOn = false;
    private int timeCounter;
    private Chronometer chronometer;
    private StopWatchState stopWatchState;
    private AppState appState;
    private ProgressBar progressBar;
    private LightTower lightTower;
    private TextView aboutMessageView;
    private RtlTimeEditText greenTime_EditText, yellowTime_EditText, redTime_EditText;
    private ImageView speaker_button, edit_button, start_button, reset_button;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        originalVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                new AlertDialog.Builder(this)
                        .setTitle("About Time-A-Light")
                        .setIcon(0)
                        .setView(aboutMessageView)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        if(stopWatchState == StopWatchState.RUNNING_COLD || stopWatchState == StopWatchState.RUNNING_HOT){
            enterStopWatchState(StopWatchState.STOPPED);
        }

        audio.setStreamVolume(AudioManager.STREAM_MUSIC, originalVolume, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        chronometer = (Chronometer) findViewById(R.id.chronometer);
        chronometer.setOnChronometerTickListener(this);
        stopWatchState= StopWatchState.IDLE;
        start_button = (ImageView) findViewById(R.id.start_button);
        reset_button = (ImageView) findViewById(R.id.reset_button);
        edit_button = (ImageView) findViewById(R.id.editImage);
        speaker_button = (ImageView) findViewById(R.id.speakerImage);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        greenTime_EditText = (RtlTimeEditText) findViewById(R.id.greenTime_EditText);
        yellowTime_EditText = (RtlTimeEditText) findViewById(R.id.yellowTime_EditText);
        redTime_EditText = (RtlTimeEditText) findViewById(R.id.redTime_EditText);

        lightTower = LightTower.getInstance();

        SharedPreferences sharedpreferences = getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        String greenTimePref = sharedpreferences.getString(GREEN_TIME_KEY, "");
        String yellowTimePref = sharedpreferences.getString(YELLOW_TIME_KEY, "");
        String redTimePref = sharedpreferences.getString(RED_TIME_KEY, "");

        if(settingsExistedAndValid(greenTimePref, yellowTimePref, redTimePref)){
            greenTime_EditText.setText(greenTimePref);
            yellowTime_EditText.setText(yellowTimePref);
            redTime_EditText.setText(redTimePref);
        }

        updateTimeSettingsCache();

        buzzerOn = sharedpreferences.getBoolean(BUZZER_KEY, false);

        lightTower.setBuzzerState( (buzzerOn ? BuzzerState.ON : BuzzerState.OFF ));

        enterStopWatchState(StopWatchState.IDLE);
        enterAppState(AppState.PLAY);

        final Activity act = this;
        greenTime_EditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });

        yellowTime_EditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                return false;
            }
        });

        redTime_EditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    Log.i("DONE", "RED FIRED");

                    //clear focus from textview
                    v.clearFocus();

                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager)act.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(redTime_EditText.getWindowToken(), 0);
                }
                return false;
            }
        });

        //for about dialog
        if(aboutMessageView == null) {
            aboutMessageView = new TextView(this);
            aboutMessageView.setText("\nThis app is brought to you by\n" +
                    "agbullet.com\n" +
                    "in partnership with\n" +
                    "Time-A-Light\n");
            aboutMessageView.setTextSize(16);
            aboutMessageView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    private boolean settingsExistedAndValid(final String greenTimePref, final String yellowTimePref, final String redTimePref) {
        return TimeUtils.isValidTimeProfile(greenTimePref, yellowTimePref, redTimePref);
    }

    private void updateProgressBarLimit() {
        progressBar.setMax(redTime_EditText.getValueInSeconds());
    }

    public void speakerClick(View view) {
        if (appState == AppState.EDIT) {
            buzzerOn = !buzzerOn;
            updateBuzzerIcon(appState, buzzerOn);
        }
    }

    public void start_buttonClick(View v) {
        if(appState == AppState.EDIT){
            return;
        }

        switch(stopWatchState){
            case IDLE:
                enterStopWatchState(StopWatchState.RUNNING_COLD);
                break;
            case STOPPED:
                enterStopWatchState(StopWatchState.RUNNING_HOT);
                break;
            case RUNNING_HOT:
            case RUNNING_COLD:
                enterStopWatchState(StopWatchState.STOPPED);
                break;
        }
    }

    public void reset_buttonClick(View v) {
        if(appState == AppState.EDIT){
            return;
        }

        switch(stopWatchState){
            case IDLE:
            case RUNNING_HOT:
            case RUNNING_COLD:
                break;
            case STOPPED:
                enterStopWatchState(StopWatchState.IDLE);
                break;
        }
    }

    private void updateBuzzerIcon(final AppState state, final boolean buzzerOn){
        switch(state){
            case EDIT:
                if(buzzerOn){
                    speaker_button.setImageResource(R.drawable.sound_on);
                } else {
                    speaker_button.setImageResource(R.drawable.sound_off);
                }
                break;
            case PLAY:
                if(buzzerOn){
                    speaker_button.setImageResource(R.drawable.sound_on_disabled);
                } else {
                    speaker_button.setImageResource(R.drawable.sound_off_disabled);
                }
                break;
        }
    }

    private void enterStopWatchState(StopWatchState state){
        switch(state){
            case IDLE:
                chronometer.stop();
                chronometer.setBase(SystemClock.elapsedRealtime());
                timeCounter = 0;
                lightTower.setLightState(LightState.IDLE);
                break;
            case RUNNING_COLD:
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                break;
            case RUNNING_HOT:
                long intervalPaused = SystemClock.elapsedRealtime() - pausedTime;
                chronometer.setBase(chronometer.getBase() + intervalPaused);
                chronometer.start();
                break;
            case STOPPED:
                pausedTime = SystemClock.elapsedRealtime();
                chronometer.stop();
                lightTower.setLightState(LightState.IDLE);
                break;
        }

        stopWatchState = state;
        updateStopWatchControls(stopWatchState);
    }

    private void updateStopWatchControls(StopWatchState state){
        switch(state){
            case IDLE:
                progressBar.setProgress(0);
                LightState currentLightState = getLightStateFromTime(timeCounter, greenTimeCache, yellowTimeCache, redTimeCache);
                updateProgressBarColour(currentLightState, progressBar);
                updateLightDisplay(currentLightState);
                lightTower.setLightState(currentLightState);
                start_button.setImageResource(R.drawable.play);
                reset_button.setImageResource(R.drawable.reset_disabled);
                edit_button.setImageResource(R.drawable.edit);
                break;
            case RUNNING_HOT:
            case RUNNING_COLD:
                start_button.setImageResource(R.drawable.pause);
                reset_button.setImageResource(R.drawable.reset_disabled);
                edit_button.setImageResource(R.drawable.edit_disabled);
                break;
            case STOPPED:
                start_button.setImageResource(R.drawable.play);
                reset_button.setImageResource(R.drawable.reset);
                edit_button.setImageResource(R.drawable.edit);
                break;
        }
    }

    private void enterAppState(AppState state) {
        switch (state){
            case EDIT:
                start_button.setImageResource(R.drawable.play_disabled);
                reset_button.setImageResource(R.drawable.reset_disabled);
                edit_button.setImageResource(R.drawable.ok);

                greenTime_EditText.setEnabled(true);
                yellowTime_EditText.setEnabled(true);
                redTime_EditText.setEnabled(true);
                break;
            case PLAY:
                switch(stopWatchState){
                    case IDLE:
                        start_button.setImageResource(R.drawable.play);
                        reset_button.setImageResource(R.drawable.reset_disabled);
                        break;
                    case STOPPED:
                        start_button.setImageResource(R.drawable.play);
                        reset_button.setImageResource(R.drawable.reset);
                        break;
                    default:
                        break;
                }

                updateStopWatchControls(stopWatchState);
                edit_button.setImageResource(R.drawable.edit);

                greenTime_EditText.setEnabled(false);
                yellowTime_EditText.setEnabled(false);
                redTime_EditText.setEnabled(false);

                updateProgressBarLimit();
                updateTimeSettingsCache();
                break;
        }

        appState = state;
        updateBuzzerIcon(appState, buzzerOn);
    }

    @Override
    public void onChronometerTick(Chronometer chronometer) {
        if(stopWatchState == StopWatchState.IDLE || stopWatchState == StopWatchState.STOPPED){
            return;
        }

        timeCounter++;
        progressBar.incrementProgressBy(1);
        LightState currentLightState = getLightStateFromTime(timeCounter, greenTimeCache, yellowTimeCache, redTimeCache);
        updateProgressBarColour(currentLightState, progressBar);
        updateLightDisplay(currentLightState);
        lightTower.setLightState(currentLightState);
    }

    private void updateProgressBarColour(LightState lightState, ProgressBar progressBar){
        switch(lightState){
            case IDLE:
                progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_horizontal_white));
                break;
            case GREEN:
                progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_horizontal_green));
                break;
            case YELLOW:
                progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_horizontal_yellow));
                break;
            case RED:
                progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_horizontal_red));
                break;
        }
    }

    private LightState getLightStateFromTime(int currentTime, int greenTime, int yellowTime, int redTime) {
        if(currentTime < greenTime){
            return LightState.IDLE;
        } else if (currentTime < yellowTime){
            return LightState.GREEN;
        } else if (currentTime < redTime){
            return LightState.YELLOW;
        } else {
            return LightState.RED;
        }
    }

    private void updateTimeSettingsCache(){
        greenTimeCache = greenTime_EditText.getValueInSeconds();
        yellowTimeCache = yellowTime_EditText.getValueInSeconds();
        redTimeCache = redTime_EditText.getValueInSeconds();
    }

    public void edit_buttonClick(View v){
        if(stopWatchState == StopWatchState.RUNNING_COLD || stopWatchState == StopWatchState.RUNNING_HOT){
            return;
        }

        if(appState == AppState.EDIT) {
            fixAndSaveValues();
            lightTower.setBuzzerState( buzzerOn ? BuzzerState.ON : BuzzerState.OFF );
            enterAppState(AppState.PLAY);
        } else {
            enterAppState(AppState.EDIT);
        }
    }

    private void updateLightDisplay(LightState currentLightState) {
        View greenView = findViewById(R.id.greenLightSquare);
        View yellowView = findViewById(R.id.yellowLightSquare);
        View redView = findViewById(R.id.redLightSquare);

        switch (currentLightState) {
            case IDLE:
                greenView.setBackgroundResource(R.drawable.round_rect_shape_green);
                yellowView.setBackgroundResource(R.drawable.round_rect_shape_yellow);
                redView.setBackgroundResource(R.drawable.round_rect_shape_red);
                break;
            case GREEN:
                greenView.setBackgroundResource(R.drawable.round_rect_shape_green_on);
                yellowView.setBackgroundResource(R.drawable.round_rect_shape_yellow);
                redView.setBackgroundResource(R.drawable.round_rect_shape_red);
                break;
            case YELLOW:
                greenView.setBackgroundResource(R.drawable.round_rect_shape_green);
                yellowView.setBackgroundResource(R.drawable.round_rect_shape_yellow_on);
                redView.setBackgroundResource(R.drawable.round_rect_shape_red);
                break;
            case RED:
                greenView.setBackgroundResource(R.drawable.round_rect_shape_green);
                yellowView.setBackgroundResource(R.drawable.round_rect_shape_yellow);
                redView.setBackgroundResource(R.drawable.round_rect_shape_red_on);
                break;
        }
    }

    private void fixAndSaveValues() {
        int greenTime= greenTime_EditText.getValueInSeconds();
        int yellowTime= yellowTime_EditText.getValueInSeconds();
        int redTime= redTime_EditText.getValueInSeconds();

        if(!greenTime_EditText.isValidTime()){
            greenTime_EditText.setText(DEFAULT_GREEN_TIME);
            greenTime = greenTime_EditText.getValueInSeconds();
        }

        if(!yellowTime_EditText.isValidTime()){
            yellowTime_EditText.setText(DEFAULT_YELLOW_TIME);
            yellowTime= yellowTime_EditText.getValueInSeconds();
        }

        if(!redTime_EditText.isValidTime()){
            redTime_EditText.setText(DEFAULT_RED_TIME);
            redTime= redTime_EditText.getValueInSeconds();
        }

        if(TimeUtils.timeTooHighForGreen(greenTime)){
            greenTime = TimeUtils.MAX_GREEN_SECONDS;
            yellowTime = greenTime + 1;
            redTime = yellowTime + 1;
        } else if(!TimeUtils.timesAreIncreasing(greenTime, yellowTime, redTime)){
            yellowTime = greenTime + 1;
            redTime = yellowTime + 1;
        }

        String greenTimeText = TimeUtils.getTimeAsStringFromInt(greenTime);
        String yellowTimeText = TimeUtils.getTimeAsStringFromInt(yellowTime);
        String redTimeText = TimeUtils.getTimeAsStringFromInt(redTime);

        greenTime_EditText.setText(greenTimeText);
        yellowTime_EditText.setText(yellowTimeText);
        redTime_EditText.setText(redTimeText);

        SharedPreferences sharedpreferences = getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(GREEN_TIME_KEY, greenTimeText);
        editor.putString(YELLOW_TIME_KEY, yellowTimeText);
        editor.putString(RED_TIME_KEY, redTimeText);
        editor.putBoolean(BUZZER_KEY, buzzerOn);
        editor.apply();
    }
}
