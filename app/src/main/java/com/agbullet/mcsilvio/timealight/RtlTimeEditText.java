package com.agbullet.mcsilvio.timealight;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

/**
 * An extension of the text entry GUI element
 */
public class RtlTimeEditText extends EditText implements View.OnClickListener, TextWatcher {
    public RtlTimeEditText(Context context) {
        super(context);
        this.setOnClickListener(this);
        this.addTextChangedListener(this);
    }

    public RtlTimeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnClickListener(this);
        this.addTextChangedListener(this);
    }

    public RtlTimeEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setOnClickListener(this);
        this.addTextChangedListener(this);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if(focused){
            setSelectionAtEnd();
            this.requestFocus();
        }
    }

    @Override
    public void onClick(View view) {
        setSelectionAtEnd();
        requestFocus();
    }

    private void setSelectionAtEnd() {
        int length = getText().length();
        setSelection(length);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        requestFocus();
        return super.onTouchEvent(event);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        //do nothing (no super)
    }

    @Override
    public void afterTextChanged(Editable field) {
        String result = field.toString();

        //remove colons
        int colonIndex = -1;
        do{
            colonIndex = result.indexOf(':');

            if(colonIndex != -1){
                result = result.substring(0, colonIndex) + result.substring(colonIndex + 1, result.length());
            }

        }while(colonIndex != -1);

        //adjust numbers
        if(result.length() > 4){
            result = result.substring(1, result.length());
        } else if (result.length() < 4){
            String leadingZeroes = "";
            for(int x = 0; x < 4 - result.length(); x++){
                leadingZeroes += "0";
            }
            result = leadingZeroes + result;
        }

        //add colon back in right spot
        result = result.substring(0, 2) + ":" + result.substring(2, 4);

        //clear field and update
        this.removeTextChangedListener(this);

        field.clear();
        field.append(result);

        this.addTextChangedListener(this);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // do nothing (no super)
    }

    public int getValueInSeconds(){
        String timetext = getText().toString();
        return TimeUtils.getTimeInSeconds(timetext);
    }

    public boolean isValidTime(){
        return TimeUtils.isValidTimeString(getText().toString());
    }
}
